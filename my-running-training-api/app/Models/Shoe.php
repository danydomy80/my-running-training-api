<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Brand;
use App\Models\Tipologie;


class Shoe extends Model
{
    use HasFactory;
    
    public function shoeModel(){
        return $this->belongsTo(ShoeModel::class, 'modello_id');
    }
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function trainings(){ 
        return $this->hasMany(Training::class, 'scarpa_id'); 
    }


}
