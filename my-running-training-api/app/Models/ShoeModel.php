<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoeModel extends Model
{
    use HasFactory;
    
    public function brand(){
        return $this->belongsTo(Brand::class, 'marca_id');
    }
    
    public function tipologia(){
        return $this->belongsTo(Tipologie::class, 'tipologia_id'); //(Tipologie::class, author_id, id) (tabella da puntare, foreign key della presente tabella, owner key dell'altra tabella)
    }

    public function shoes(){ 
        return $this->hasMany(Shoe::class, 'modello_id'); // (Shoe::class, tipologia_id, id) (tabella da puntare, foreign key dell'altra tabella, local key di questa tabella)
    }

}
