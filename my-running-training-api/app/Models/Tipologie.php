<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shoe;

class Tipologie extends Model
{
    use HasFactory;

    
    public function shoeModels(){ 
        return $this->hasMany(ShoeModel::class, 'tipologia_id'); // (ShoeModels::class, tipologia_id, id) (tabella da puntare, foreign key dell'altra tabella, local key di questa tabella)
    }
}
