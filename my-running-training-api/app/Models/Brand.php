<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shoe;

class Brand extends Model
{
    use HasFactory;

    public function shoeModels(){ 
        return $this->hasMany(ShoeModel::class, 'marca_id'); // (ShoeModel::class, marca_id, id) (tabella da puntare, foreign key dell'altra tabella, local key di questa tabella)
    }
}
