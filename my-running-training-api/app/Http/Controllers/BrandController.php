<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //per utilizzare classe Validator
use App\Models\Brand;


class BrandController extends Controller
{
    public function getBrands(){
        //passare tutti i record della tabella brands
        $brands = Brand::get();
        return response()->json($brands, 200);
    }

    public function getSingleBrand($id){
    //passare un determinato record della tabella brands
        $brand = Brand::with('shoemodels')->findOrFail($id); //findOrFail a differenza del find normale evita di fare una validazione, la fa lui
        return response()->json($brand, 200);
    }

    public function createBrand(Request $request){
        //validare l'input
        $validator = Validator::make($request->all(), [
            'marca' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        $brand = new Brand;
        $brand->marca = $request->input('marca');
        $brand->save();
        
        //emettere una risposta
        return response()->json($brand, 201);
    }
    
    public function updateBrand($id, Request $request){
        $brand = Brand::findOrFail($id);
        //validare l'input
        $validator = Validator::make($request->all(), [
            'marca' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        $brand->marca = $request->input('marca');
        $brand->save();

        //emettere una risposta
        return response()->json($brand, 200);
    }
    
    public function partialUpdateBrand($id, Request $request){
        $brand = Brand::findOrFail($id);
        //validare l'input
        $validator = Validator::make($request->all(), [
            'marca' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        if($request->has('marca')){
            $brand->marca = $request->input('marca');
        }
        $brand->save();

        //emettere una risposta
        return response()->json($brand, 200);
    }

    public function deleteBrand($id){
        //eliminare un determinato record dalla tabella books
        $brand = Brand::findOrFail($id);
        $brand->delete();
        return response()->json(null, 204);
    }
}
