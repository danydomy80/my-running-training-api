<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //per utilizzare classe Validator
use App\Models\ShoeModel;

class ShoeModelController extends Controller
{
    public function getShoeModels(){
        //passare tutti i record della tabella shoes
        $shoeModels = ShoeModel::get();
        return response()->json($shoeModels, 200);
    }

    public function getSingleShoeModel($id){
    //passare un determinato record della tabella shoe_models
        $shoeModel = ShoeModel::with('brand')->with('tipologia')->findOrFail($id); //findOrFail a differenza del find normale evita di fare una validazione, la fa lui
        return response()->json($shoeModel, 200);
    }

    public function createShoeModel(Request $request){
        //validare l'input
        $validator = Validator::make($request->all(), [
            'marca_id' => 'required|exists:brands,id',
            'modello' => 'required|max:255',
            'tipologia_id' => 'required|exists:tipologies,id'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        $shoeModel = new ShoeModel;
        $shoeModel->marca_id = $request->input('marca_id');
        $shoeModel->modello = $request->input('modello');
        $shoeModel->tipologia_id = $request->input('tipologia_id');
        $shoeModel->save();
        
        //emettere una risposta
        return response()->json($shoeModel, 201);
    }
    
    public function updateShoeModel($id, Request $request){
        $shoeModel = ShoeModel::findOrFail($id);
        //validare l'input
        $validator = Validator::make($request->all(), [
            'marca_id' => 'required|exists:brands,id',
            'modello' => 'required|max:255',
            'tipologia_id' => 'required|exists:tipologies,id'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        $shoeModel->marca_id = $request->input('marca_id');
        $shoeModel->modello = $request->input('modello');
        $shoeModel->tipologia_id = $request->input('tipologia_id');
        $shoeModel->save();

        //emettere una risposta
        return response()->json($shoeModel, 200);
    }
    
    public function partialUpdateShoeModel($id, Request $request){
        $shoeModel = ShoeModel::findOrFail($id);
        //validare l'input
        $validator = Validator::make($request->all(), [
            'marca_id' => 'sometimes|required|exists:brands,id',
            'modello' => 'sometimes|required|max:255',
            'tipologia_id' => 'sometimes|required|exists:tipologies,id'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        if($request->has('marca_id')){
            $shoeModel->marca_id = $request->input('marca_id');
        }
        if($request->has('modello')){
            $shoeModel->modello = $request->input('modello');
        }
        if($request->has('tipologia_id')){
            $shoeModel->tipologia_id = $request->input('tipologia_id');
        }
        $shoeModel->save();

        //emettere una risposta
        return response()->json($shoeModel, 200);
    }

    public function deleteShoeModel($id){
        //eliminare un determinato record dalla tabella books
        $shoeModel = ShoeModel::findOrFail($id);
        $shoeModel->delete();
        return response()->json(null, 204);
    }
}
