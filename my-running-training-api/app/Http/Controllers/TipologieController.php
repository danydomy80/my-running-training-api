<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //per utilizzare classe Validator
use App\Models\Tipologie;

class TipologieController extends Controller
{
    public function getTipologies(){
        //passare tutti i record della tabella tipologies
        $tipologies = Tipologie::get();
        return response()->json($tipologies, 200);
    }

    public function getSingleTipologie($id){
    //passare un determinato record della tabella tipologies
        $tipologie = Tipologie::with('shoemodels')->findOrFail($id); //findOrFail a differenza del find normale evita di fare una validazione, la fa lui
        return response()->json($tipologie, 200);
    }

    public function createTipologie(Request $request){
        //validare l'input
        $validator = Validator::make($request->all(), [
            'tipologia' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        $tipologie = new Tipologie;
        $tipologie->tipologia = $request->input('tipologia');
        $tipologie->save();
        
        //emettere una risposta
        return response()->json($tipologie, 201);
    }
    
    public function updateTipologie($id, Request $request){
        $tipologie = Tipologie::findOrFail($id);
        //validare l'input
        $validator = Validator::make($request->all(), [
            'tipologia' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        $tipologie->tipologia = $request->input('tipologia');
        $tipologie->save();

        //emettere una risposta
        return response()->json($tipologie, 200);
    }
    
    public function partialUpdateTipologie($id, Request $request){
        $tipologie = Tipologie::findOrFail($id);
        //validare l'input
        $validator = Validator::make($request->all(), [
            'tipologia' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response() -> json ([
                'errors' => $validator->errors()
            ], 400);
        }
        
        //inserire il record
        if($request->has('tipologia')){
            $tipologie->tipologia = $request->input('tipologia');
        }
        $tipologie->save();

        //emettere una risposta
        return response()->json($tipologie, 200);
    }

    public function deleteTipologie($id){
        //eliminare un determinato record dalla tabella books
        $tipologie = Tipologie::findOrFail($id);
        $tipologie->delete();
        return response()->json(null, 204);
    }
}
