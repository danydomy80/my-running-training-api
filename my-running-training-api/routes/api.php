<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/shoeModels', 
    'App\Http\Controllers\ShoeModelController@getShoeModels'); //rotta x ottenere tutti i modelli
Route::get('/shoeModels/{id}', 
    'App\Http\Controllers\ShoeModelController@getSingleShoeModel'); //rotta x ottenere un modello specifico

Route::post('/shoeModels', 
    'App\Http\Controllers\ShoeModelController@createShoeModel'); //la funzione si può chiamare anche save o store, l'importante è che sia chiara la logica che esprime

Route::put('/shoeModels/{id}', 
'App\Http\Controllers\ShoeModelController@updateShoeModel');

Route::patch('/shoeModels/{id}', 
'App\Http\Controllers\ShoeModelController@partialUpdateShoeModel');

Route::delete('/shoeModels/{id}', 
'App\Http\Controllers\ShoeModelController@deleteShoeModel');

Route::get('/brands', 
    'App\Http\Controllers\BrandController@getBrands'); 
Route::get('/brands/{id}', 
    'App\Http\Controllers\BrandController@getSingleBrand'); 

Route::post('/brands', 
    'App\Http\Controllers\BrandController@createBrand'); 

Route::put('/brands/{id}', 
'App\Http\Controllers\BrandController@updateBrand');

Route::patch('/brands/{id}', 
'App\Http\Controllers\BrandController@partialUpdateBrand');

Route::delete('/brands/{id}', 
'App\Http\Controllers\BrandController@deleteBrand');


Route::get('/tipologies', 
    'App\Http\Controllers\TipologieController@getTipologies'); 
Route::get('/tipologies/{id}', 
    'App\Http\Controllers\TipologieController@getSingleTipologie'); 

Route::post('/tipologies', 
    'App\Http\Controllers\TipologieController@createTipologie'); 

Route::put('/tipologies/{id}', 
'App\Http\Controllers\TipologieController@updateTipologie');

Route::patch('/tipologies/{id}', 
'App\Http\Controllers\TipologieController@partialUpdateTipologie');

Route::delete('/tipologies/{id}', 
'App\Http\Controllers\TipologieController@deleteTipologie');
