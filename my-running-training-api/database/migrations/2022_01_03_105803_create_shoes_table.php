<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('modello_id');
            $table->unsignedBigInteger('user_id');
            $table->string('alias')->nullable(); //VARCHAR
            $table->text('commenti')->nullable(); //TEXT
            $table->year('anno_inizio_utilizzo'); //YEAR
            $table->year('anno_fine_utilizzo')->nullable(); //YEAR
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoes');
    }
}
